## This app has following dependencies:

- redux
- redux-router
- redux-saga
- enzyme


## To start

`yarn` or `npm install`
`yarn start` or `npm start`