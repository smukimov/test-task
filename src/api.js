import db from './seeds/data.json';
import {Merchant} from './entities/merchant';
import {Bid} from './entities/bid';
import * as moment from 'moment';

export class Api {
  static async fetchMerchants(pageNum, itemsPerPage = 10) {
    return await {
      merchants: db.slice((pageNum - 1) * itemsPerPage, pageNum * itemsPerPage).map(data => new Merchant(data)),
      totalPages: Math.round(db.length / itemsPerPage),
    };
  }

  static async updateMerchant(id, merchantFrom) {
    const merchantTo = db.find(m => m.id === id);
    if (!merchantTo) {
      throw Error(`Error: Merchant with id=${id} is not found`);
    }
    merchantTo.firstname = merchantFrom.firstname;
    merchantTo.lastname = merchantFrom.lastname;
    merchantTo.email = merchantFrom.email;
    merchantTo.phone = merchantFrom.phone;
    merchantTo.hasPremium = merchantFrom.hasPremium;
    return await {};
  }

  static async createMerchant(merchant) {
    merchant.id = db.length;
    db.unshift(merchant);
    return await {};
  }

  static async getMerchant(id) {
    const data = db.find(m => m.id === id);
    if (!data) {
      throw Error(`Error: Merchant with id=${id} is not found`);
    }
    return await {merchant: new Merchant(data)};
  }

  static async deleteMerchant(id) {
    const merchantIndex = db.findIndex(m => m.id === id);
    if (merchantIndex === -1) {
      throw Error(`Error: Merchant with id=${id} is not found`);
    }
    db.splice(merchantIndex, 1);
    return await {};
  }

  static async fetchBids(merchantId) {
    const data = db.find(m => m.id === merchantId);
    if (!data) {
      throw Error(`Error: Merchant with id=${merchantId} is not found`);
    }
    return await {
      bids: data.bids
        .sort((a, b) => {
          if (moment(a.created).isAfter(b.created)) {
            return -1;
          }
          if (moment(a.created).isBefore(b.created)) {
            return 1;
          }
          return 0;
        })
        .map(b => new Bid(b)),
    };
  }
}
