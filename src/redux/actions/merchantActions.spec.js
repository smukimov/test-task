import * as types from '../constants';
import * as actions from './merchantActions';
import {Merchant} from '../../entities/merchant';

describe('merchant actions', () => {
  it('should create an action to request a merchants', () => {
    const pageNum = 2;
    const expectedAction = {
      type: types.MERCHANTS_FETCH_REQUESTED,
      payload: {pageNum},
    };
    expect(actions.merchantsFetchRequest({pageNum})).toEqual(expectedAction);
  });

  it('should create an action to request a merchant update', () => {
    const merchantId = 2;
    const merchant = new Merchant();
    const expectedAction = {
      type: types.MERCHANT_UPDATE_REQUESTED,
      payload: {merchantId, merchant},
    };
    expect(actions.merchantUpdateRequest({merchantId, merchant})).toEqual(expectedAction);
  });

  it('should create an action to request a merchant delete', () => {
    const merchantId = 2;
    const expectedAction = {
      type: types.MERCHANT_DELETE_REQUESTED,
      payload: {merchantId},
    };
    expect(actions.merchantDeleteRequest({merchantId})).toEqual(expectedAction);
  });

  it('should create an action to request a merchant get', () => {
    const merchantId = 2;
    const expectedAction = {
      type: types.MERCHANT_GET_REQUESTED,
      payload: {merchantId},
    };
    expect(actions.merchantGetRequest({merchantId})).toEqual(expectedAction);
  });

  it('should create an action to request a merchant create', () => {
    const merchant = new Merchant();
    const expectedAction = {
      type: types.MERCHANT_CREATE_REQUESTED,
      payload: {merchant},
    };
    expect(actions.merchantCreateRequest({merchant})).toEqual(expectedAction);
  });
});
