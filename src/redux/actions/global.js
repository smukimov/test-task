import * as types from '../constants';

export function showErrorMessage({message}) {
  return {
    type: types.ERROR_MESSAGE_SHOW,
    payload: {message},
  };
}
