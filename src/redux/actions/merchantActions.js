import * as types from '../constants';

export function merchantsFetchRequest({pageNum}) {
  return {
    type: types.MERCHANTS_FETCH_REQUESTED,
    payload: {pageNum},
  };
}

export function merchantUpdateRequest({merchantId, merchant}) {
  return {
    type: types.MERCHANT_UPDATE_REQUESTED,
    payload: {merchantId, merchant},
  };
}

export function merchantUpdateSucceeded() {
  return {
    type: types.MERCHANT_UPDATE_SUCCEEDED,
    payload: {},
  };
}

export function merchantCreateRequest({merchant}) {
  return {
    type: types.MERCHANT_CREATE_REQUESTED,
    payload: {merchant},
  };
}

export function merchantCreateSucceeded() {
  return {
    type: types.MERCHANT_CREATE_SUCCEEDED,
    payload: {},
  };
}

export function merchantGetRequest({merchantId}) {
  return {
    type: types.MERCHANT_GET_REQUESTED,
    payload: {merchantId},
  };
}

export function merchantGetSucceeded({merchant}) {
  return {
    type: types.MERCHANT_GET_SUCCEEDED,
    payload: {merchant},
  };
}

export function merchantDeleteRequest({merchantId}) {
  return {
    type: types.MERCHANT_DELETE_REQUESTED,
    payload: {merchantId},
  };
}

export function merchantDeleteSucceeded() {
  return {
    type: types.MERCHANT_DELETE_SUCCEEDED,
  };
}

export function merchantsFetchSucceeded({merchants, pageNum, totalPages}) {
  return {
    type: types.MERCHANTS_FETCH_SUCCEEDED,
    payload: {merchants, pageNum, totalPages},
  };
}
