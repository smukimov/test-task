import * as actions from './bidActions';
import * as types from '../constants';
import {showErrorMessage} from './global';

describe('bid actions', () => {
  it('should create an action to request succeed', () => {
    const bids = [];
    const expectedAction = {
      type: types.BID_FETCH_SUCCEEDED,
      payload: {bids},
    };
    expect(actions.bidFetchSucceeded({bids})).toEqual(expectedAction);
  });

  it ('should create an action to bids request', () => {
    const merchantId = 1;
    const expectedAction = {
      type: types.BID_FETCH_REQUESTED,
      payload: {merchantId},
    };
    expect(actions.bidFetchRequest({merchantId})).toEqual(expectedAction);
  });

  it ('should create an action to request failed', () => {
    const message = 'error';
    const expectedAction = {
      type: types.ERROR_MESSAGE_SHOW,
      payload: {message},
    };
    expect(showErrorMessage({message})).toEqual(expectedAction);
  });
});
