import * as types from '../constants';

export function bidFetchSucceeded({bids}) {
  return {
    type: types.BID_FETCH_SUCCEEDED,
    payload: {bids},
  };
}

export function bidFetchRequest({merchantId}) {
  return {
    type: types.BID_FETCH_REQUESTED,
    payload: {merchantId},
  };
}
