import * as types from '../constants';
import * as actions from '../actions/bidActions';
import {call, put, all, takeEvery} from 'redux-saga/effects';
import {Api} from '../../api';
import {showErrorMessage} from '../actions/global';

const defaultState = {
  bids: [],
  isLoading: false,
  message: null,
};

export function* getAll({payload: {merchantId}}) {
  try {
    const {bids} = yield call(Api.fetchBids, merchantId);
    yield put(actions.bidFetchSucceeded({bids}));
  } catch ({message}) {
    yield put(showErrorMessage({message}));
  }
}

export function* watchAll() {
  yield all([
    yield takeEvery(types.BID_FETCH_REQUESTED, getAll),
  ]);
}

export default function (state = defaultState, action) {
  switch (action.type) {
    case types.BID_FETCH_REQUESTED: {
      return {...state, ...{bids: [], isLoading: true}};
    }
    case types.BID_FETCH_SUCCEEDED: {
      const {bids} = action.payload;

      return {...state, ...{bids, isLoading: false}};
    }
    case types.ERROR_MESSAGE_SHOW: {
      const {message} = action.payload;
      return {...state, ...{isLoading: false, error: message}};
    }
    default:
      return state;
  }
}
