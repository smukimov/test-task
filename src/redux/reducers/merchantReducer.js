import {call, put, takeEvery, all} from 'redux-saga/effects';

import {Api} from '../../api';
import * as types from '../constants';
import * as actions from '../actions/merchantActions';
import {showErrorMessage} from '../actions/global';

export function* getAll({payload: {pageNum}}) {
  try {
    const {merchants, totalPages} = yield call(Api.fetchMerchants, pageNum);
    yield put(actions.merchantsFetchSucceeded({merchants, pageNum, totalPages}));
  } catch ({message}) {
    yield put(showErrorMessage({message}));
  }
}

export function* updateMerchant({payload: {merchantId, merchant}}) {
  try {
    yield call(Api.updateMerchant, merchantId, merchant);
    yield put(actions.merchantUpdateSucceeded());
  } catch ({message}) {
    yield put(showErrorMessage({message}));
  }
}

export function* createMerchant({payload: {merchant}}) {
  try {
    yield call(Api.createMerchant, merchant);
    yield put(actions.merchantCreateSucceeded());
  } catch ({message}) {
    yield put(showErrorMessage({message}));
  }
}

export function* deleteMerchant({payload: {merchantId}}) {
  try {
    yield call(Api.deleteMerchant, merchantId);
    yield put(actions.merchantDeleteSucceeded());
    yield put(actions.merchantsFetchRequest({pageNum: 1}));
  } catch ({message}) {
    yield put(showErrorMessage({message}));
  }
}

export function* getById({payload: {merchantId}}) {
  try {
    const {merchant} = yield call(Api.getMerchant, merchantId);
    yield put(actions.merchantGetSucceeded({merchant}));
  } catch ({message}) {
    yield put(showErrorMessage({message}));
  }
}

export function* watchAll() {
  yield all([
    yield takeEvery(types.MERCHANT_GET_REQUESTED, getById),
    yield takeEvery(types.MERCHANTS_FETCH_REQUESTED, getAll),
    yield takeEvery(types.MERCHANT_UPDATE_REQUESTED, updateMerchant),
    yield takeEvery(types.MERCHANT_CREATE_REQUESTED, createMerchant),
    yield takeEvery(types.MERCHANT_DELETE_REQUESTED, deleteMerchant),
  ]);
}

const defaultState = {
  merchants: [],
  merchant: null,
  currentPage: 1,
  totalPages: 0,
  isLoading: false,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    case types.MERCHANTS_FETCH_SUCCEEDED: {
      const {pageNum, merchants, totalPages} = action.payload;
      const nextState = {
        currentPage: pageNum,
        merchants,
        totalPages,
        isLoading: false,
      };
      return {...state, ...nextState};
    }
    case types.MERCHANT_GET_SUCCEEDED: {
      const {merchant} = action.payload;
      const nextState = {
        merchant,
        isLoading: false,
      };
      return {...state, ...nextState};
    }
    case types.MERCHANT_DELETE_SUCCEEDED:
    case types.MERCHANT_UPDATE_SUCCEEDED:
    case types.MERCHANT_CREATE_SUCCEEDED: {
      return {...state, ...{isLoading: false}};
    }
    case types.MERCHANTS_FETCH_REQUESTED: {
      return {...state, ...{isLoading: true}};
    }
    case types.MERCHANT_GET_REQUESTED:
    case types.MERCHANT_UPDATE_REQUESTED: {
      return {...state, ...{isLoading: true, merchant: null}};
    }
    case types.ERROR_MESSAGE_SHOW: {
      const {message} = action.payload;
      return {...state, ...{isLoading: false, error: message}};
    }
    default:
      return state;
  }
};
