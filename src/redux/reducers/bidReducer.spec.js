import reducer from './bidReducer';
import * as actions from '../actions/bidActions';

describe('bid reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        bids: [],
        isLoading: false,
        message: null,
      }
    );
  });

  it('should should handle BIDS_FETCH_SUCCEEDED', () => {
    expect(
      reducer({}, actions.bidFetchSucceeded({bids: []}))
    ).toEqual(
      {
        bids: [],
        isLoading: false,
      }
    );
  });
});
