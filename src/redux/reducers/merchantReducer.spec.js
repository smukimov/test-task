import reducer from './merchantReducer';
import * as saga from './merchantReducer';
import {Merchant} from '../../entities/merchant';
import {call, put} from 'redux-saga/effects';
import {Api} from '../../api';
import * as actions from '../actions/merchantActions';

describe('merchant reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        merchants: [],
        merchant: null,
        currentPage: 1,
        totalPages: 0,
        isLoading: false,
      }
    );
  });

  it('should handle MERCHANTS_FETCH_SUCCEEDED', () => {
    expect(
      reducer({}, actions.merchantsFetchSucceeded({merchants: [], pageNum: 1, totalPages: 1}))
    ).toEqual(
      {
        merchants: [],
        currentPage: 1,
        totalPages: 1,
        isLoading: false,
      }
    );
  });

  it('should handle MERCHANTS_GET_SUCCEEDED', () => {
    const merchant = new Merchant();
    expect(
      reducer({}, actions.merchantGetSucceeded({merchant}))
    ).toEqual(
      {
        merchant,
        isLoading: false,
      }
    );
  });

  it('should handle MERCHANT_DELETE_SUCCEEDED', () => {
    expect(
      reducer({}, actions.merchantDeleteSucceeded())
    ).toEqual(
      {
        isLoading: false,
      }
    );
  });

  it('should handle MERCHANTS_FETCH_REQUESTED', () => {
    const gen = saga.getAll({payload: {pageNum: 1}});
    expect(gen.next().value).toEqual(call(Api.fetchMerchants, 1));
    const payload = {merchants: [], pageNum: 1, totalPages: 0};
    expect(gen.next(payload).value).toEqual(put(actions.merchantsFetchSucceeded(payload)));
  });

  it('should handle MERCHANT_GET_REQUESTED', () => {
    const gen = saga.getById({payload: {merchantId: 1}});
    expect(gen.next().value).toEqual(call(Api.getMerchant, 1));
    const payload = {merchant: new Merchant()};
    expect(gen.next(payload).value).toEqual(put(actions.merchantGetSucceeded(payload)));
  });

  it('should handle MERCHANT_UPDATE_REQUESTED', () => {
    const merchant = new Merchant();
    const gen = saga.updateMerchant({payload: {merchantId: 1, merchant}});
    expect(gen.next().value).toEqual(call(Api.updateMerchant, 1, merchant));
    expect(gen.next().value).toEqual(put(actions.merchantUpdateSucceeded()));
  });

  it('should handle MERCHANT_DELETE_REQUESTED', () => {
    const gen = saga.deleteMerchant({payload: {merchantId: 1}});
    expect(gen.next().value).toEqual(call(Api.deleteMerchant, 1));
    expect(gen.next().value).toEqual(put(actions.merchantDeleteSucceeded()));
  });

  it('should handle MERCHANT_CREATE_REQUESTED', () => {
    const merchant = new Merchant();
    const gen = saga.createMerchant({payload: {merchant}});
    expect(gen.next().value).toEqual(call(Api.createMerchant, merchant));
    expect(gen.next().value).toEqual(put(actions.merchantCreateSucceeded()));
  });
});
