import {createStore, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {all} from 'redux-saga/effects';
import logger from 'redux-logger';

import merchantReducer, {watchAll as watchMerchants} from './reducers/merchantReducer';
import bidReducer, {watchAll as watchBids} from './reducers/bidReducer';

const sagaMiddleware = createSagaMiddleware();

function* watchAll() {
  yield all([watchMerchants(), watchBids()]);
}

export function configureStore() {
  const store = createStore(combineReducers({m: merchantReducer, b: bidReducer}), applyMiddleware(logger, sagaMiddleware));
  sagaMiddleware.run(watchAll);
  return store;
}
