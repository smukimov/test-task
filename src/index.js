import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';

import 'normalize.css';
import './index.css';
import './styles/form.css';
import './styles/button.css';
import './styles/button-group.css';
import './styles/tables.css';

import {App} from './containers/App';

ReactDOM.render(<Router><App /></Router>, document.getElementById('root'));
