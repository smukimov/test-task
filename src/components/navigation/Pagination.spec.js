import {Pagination} from './Pagination';
import React from 'react';
import {shallow} from 'enzyme';
import {PageItem} from './PageItem';

describe('<Pagination />', () => {
  it('renders without crash', () => {
    const pagination = shallow(
      <Pagination currentPage={1} totalPages={2} onPageChange={jest.fn()} />
    );
    expect(pagination.is('.Pagination')).toBeTruthy();
  });

  it('renders Previous and Next <PageItem />', () => {
    const pagination = shallow(
      <Pagination currentPage={2} totalPages={3} onPageChange={jest.fn()} />
    );
    expect(pagination.find(PageItem)).toHaveLength(5);
  });
});
