import React from 'react';
import PropTypes from 'prop-types';

import './Pagination.css';
import {PageItem} from './PageItem';

const MAX_BLOCKS = 11;
const MAX_PIVOT_PAGES = Math.round((MAX_BLOCKS - 5) / 2);
const ELLIP = '&hellip;';

export class Pagination extends React.PureComponent {

  static propTypes = {
    currentPage: PropTypes.number.isRequired,
    totalPages: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
  };

  static getPages({currentPage, totalPages}) {
    const pages = [];

    function buildPage(label, value) {
      return {
        key: `page-${label}-${value}`,
        label,
        value,
      };
    }

    if (totalPages > 1) {
      const commonMin = Math.max(2, currentPage - MAX_PIVOT_PAGES);
      const maxPage = Math.min(totalPages - 1, currentPage + MAX_PIVOT_PAGES * 2 - (currentPage - commonMin));
      const minPage = Math.max(2, commonMin - (MAX_PIVOT_PAGES * 2 - (maxPage - commonMin)));

      pages.push(buildPage('1', 1));

      for (let i = minPage; i <= maxPage; i++) {
        const hasLess = i === minPage && i !== 2;
        const hasMore = i === maxPage && i !== totalPages - 1;

        pages.push(hasMore || hasLess
          ? buildPage(ELLIP, hasMore ? i + 2 : i - 2)
          : buildPage(String(i), i));
      }

      pages.push(buildPage(String(totalPages), totalPages));
    }
    return pages;
  }


  handleOnClick = pageNum => {
    this.props.onPageChange({pageNum});
  };

  render() {
    const {currentPage, totalPages} = this.props;
    const pages = Pagination.getPages({currentPage, totalPages});

    return (
      <div className="Pagination">
        <PageItem className="Pagination__item--previous" label="Previous" onClick={this.handleOnClick}
                  value={currentPage - 1}
                  isDisabled={currentPage === 1} />

        {pages.map(p =>
          <PageItem key={p.key} label={p.label} onClick={this.handleOnClick}
                    value={p.value} isDisabled={currentPage === p.value} />
        )}

        <PageItem className="Pagination__item--next" label="Next" onClick={this.handleOnClick} value={currentPage + 1}
                  isDisabled={currentPage === totalPages} />
      </div>
    );
  }
}
