import {shallow} from 'enzyme';
import {PageItem} from './PageItem';
import React from 'react';

describe('<PageItem />', () => {
  it('renders without crash', () => {
    const pageItem = shallow(
      <PageItem label="foo" onClick={jest.fn()} value={1} />
    );
    expect(pageItem.is('.Pagination__item')).toBeTruthy();
  });

  it('calls onClick', () => {
    const onClickCallback = jest.fn();
    const pageItem = shallow(
      <PageItem label="foo" onClick={onClickCallback} value={1} />
    );
    pageItem.simulate('click');
    expect(onClickCallback.mock.calls).toHaveLength(1);
  });

  it('have class Pagination__item--disabled', () => {
    const pageItem = shallow(
      <PageItem label="foo" onClick={jest.fn()} value={1} isDisabled />
    );
    expect(pageItem.hasClass('Pagination__item--disabled')).toBeTruthy();
  });
});
