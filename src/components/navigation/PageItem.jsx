import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

export class PageItem extends React.PureComponent {
  static defaultProps = {
    isDisabled: false,
    className: '',
  };

  static propTypes = {
    label: PropTypes.string.isRequired,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired,
  };

  handleOnClick = () => {
    this.props.onClick(this.props.value);
  };

  render() {
    return (
      <a
        className={cx('Pagination__item', this.props.className, this.props.isDisabled && 'Pagination__item--disabled')}
        onClick={this.handleOnClick}
        dangerouslySetInnerHTML={{
          __html: this.props.label,
        }}>
      </a>
    );
  }
}

PageItem.defaultProps = {
  isDisabled: false,
  className: '',
};

PageItem.propTypes = {
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired,
};
