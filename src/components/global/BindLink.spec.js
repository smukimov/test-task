import {shallow} from 'enzyme';
import React from 'react';
import {BindLink} from './BindLink';

describe('<BindLink />', () => {
  it('renders without crash', () => {
    const bindLink = shallow(
      <BindLink value="foo" onClick={jest.fn()}>Foo</BindLink>
    );
    expect(bindLink.is('.g-text-link')).toBeTruthy();
  });
});