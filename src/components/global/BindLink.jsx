import React from 'react';
import PropTypes from 'prop-types';

export class BindLink extends React.Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    children: PropTypes.node,
    className: PropTypes.string,
  };

  handleOnClick = () => {
    const {onClick, value} = this.props;
    onClick(value);
  };

  render() {
    const {children, className} = this.props;
    return <a className={`${className} g-text-link`} onClick={this.handleOnClick}>{children}</a>;
  }
}
