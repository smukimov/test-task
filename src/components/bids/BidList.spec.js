import React from 'react';
import {BidList} from './BidList';
import {shallow} from 'enzyme';

describe('<BidList />', () => {
  it('renders without crash', () => {
    const bidList = shallow(
      <BidList />
    );
    expect(bidList.is('.BidList')).toBeTruthy();
  });
});
