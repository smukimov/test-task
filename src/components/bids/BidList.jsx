import React from 'react';
import PropTypes from 'prop-types';
import * as moment from 'moment';

import './BidList.css';
import {Bid} from '../../entities/bid';
import {Link} from 'react-router-dom';

export function BidList({bidList}) {
  return (
    <div className="BidList">
      <Link to='/' className="btn btn--primary">Back</Link>
      <table className="table">
        <thead>
        <tr>
          <th>Car Title</th>
          <th>Amount</th>
          <th>Created</th>
        </tr>
        </thead>
        <tbody>
        {bidList.map(b =>
          <tr key={b.id}>
            <td>{b.carTitle}</td>
            <td>{b.amount}</td>
            <td>{moment(b.created).format('DD/MMM/YY')}</td>
          </tr>
        )}
        </tbody>
      </table>
    </div>
  );
}

BidList.defaultProps = {
  bidList: [],
};

BidList.propTypes = {
  bidList: PropTypes.arrayOf(PropTypes.instanceOf(Bid).isRequired),
};
