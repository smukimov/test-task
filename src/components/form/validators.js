function required(minLength, value) { return value && value.length >= minLength; }

export const Validator = {
  '3': required.bind(null, 3),
  '4': required.bind(null, 4),
  '5': required.bind(null, 5),
  '6': required.bind(null, 6),
  email: value => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value), // eslint-disable-line
};
