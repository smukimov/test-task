import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import {Validator} from './validators';

export class Field extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.string,
  };

  handleOnChange = ({target}) => {
    const value = target.value;
    const {name} = this.props;
    this.props.onChange({name, value});
  };

  checkIsValid(value) {
    const {required} = this.props;
    if (required) {
      const isValid = Validator[required](value);
      const error = parseInt(required, 10) ? `Must contain at least ${required} chars` : `Must contain ${required}`;
      return {isValid, error};
    }
    return {isValid: true, error: ''};
  }

  render() {
    const {label, value, name, required} = this.props;
    const {isValid, error} = this.checkIsValid(value);

    const fieldCN = cn('form__field', {
      'form__field--invalid': !isValid,
    });

    const labelCN = label && cn('form__label', {
      'form__label--required': !isValid,
      'form__label--optional': !required,
    });

    return (
      <div className={fieldCN}>
        {label && <label className={labelCN}>{label}</label>}
        <div className="form__controls">
          <input className="input form-control" type="text" name={name} value={value} onChange={this.handleOnChange} />
          <div className="form__details-wrapper">
            {!isValid && <div className="form__message">{error}</div>}
          </div>
        </div>
      </div>
    );
  }
}

