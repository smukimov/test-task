import {Validator} from './validators';

it('requires 3 characters', () => {
  expect(Validator['3']('fo')).toBe(false);
  expect(Validator['3']('foo')).toBe(true);
});

it('requires 6 characters', () => {
  expect(Validator['5']('foob')).toBe(false);
  expect(Validator['5']('foobar')).toBe(true);
});

it('requires email', () => {
  expect(Validator.email('foob')).toBe(false);
  expect(Validator.email('foo@bar.com')).toBe(true);
});