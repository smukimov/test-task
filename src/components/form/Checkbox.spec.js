import React from 'react';
import {mount, shallow} from 'enzyme';
import {Checkbox} from './Checkbox';

describe('<Checkbox />', () => {
  it('renders without crash', () => {
    const checkbox = shallow(
      <Checkbox name="foo" onChange={jest.fn()} />
    );
    expect(checkbox.is('.form__field')).toBeTruthy();
  });

  it('renders label if provided', () => {
    const expectedLabel = 'Foo bar';
    const checkbox = shallow(
      <Checkbox name="foo" onChange={jest.fn()} label={expectedLabel} />
    );
    expect(checkbox.contains(<label className="form__label">{expectedLabel}</label>)).toBeTruthy();
  });

  it('calls onChange', () => {
    const onChangeCallback = jest.fn();
    const checkbox = mount(
      <Checkbox name="foo" onChange={onChangeCallback} />
    );
    checkbox.find('input').simulate('change');
    expect(onChangeCallback.mock.calls).toHaveLength(1);
  });

  it('sets name and checked', () => {
    const checkbox = shallow(
      <Checkbox name="foo" onChange={jest.fn()} checked={true} />
    );
    expect(checkbox.find('input').props()).toMatchObject({name: 'foo', checked: true});
  });
});