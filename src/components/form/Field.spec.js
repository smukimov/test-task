import React from 'react';
import {mount, shallow} from 'enzyme';
import {Field} from './Field';

describe('<Field />', () => {
  it('renders without crash', () => {
    const field = shallow(
      <Field name='foo' onChange={jest.fn()} />
    );
    expect(field.is('.form__field')).toBeTruthy();
  });

  it('renders label if provided', () => {
    const expectedLabel = 'Foo bar';
    const checkbox = shallow(
      <Field name="foo" onChange={jest.fn()} label={expectedLabel} />
    );
    expect(checkbox.contains(<label className="form__label form__label--optional">{expectedLabel}</label>)).toBeTruthy();
  });

  it('renders label asterisk if required', () => {
    const expectedLabel = 'Foo bar';
    const checkbox = shallow(
      <Field name="foo" onChange={jest.fn()} label={expectedLabel} required="3" />
    );
    expect(checkbox.contains(<label className="form__label form__label--required">{expectedLabel}</label>)).toBeTruthy();
  });

  it('calls onChange', () => {
    const onChangeCallback = jest.fn();
    const checkbox = mount(
      <Field name="foo" onChange={onChangeCallback} />
    );
    checkbox.find('input').simulate('change');
    expect(onChangeCallback.mock.calls.length).toBe(1);
  });

  it('sets name and value', () => {
    const checkbox = mount(
      <Field name="foo" onChange={jest.fn()} value="bar" />
    );
    expect(checkbox.find('input').props()).toMatchObject({name: 'foo', value: 'bar'});
  });

  it('renders error message', () => {
    const checkbox = shallow(
      <Field name="foo" onChange={jest.fn()} required="3" />
    );
    expect(checkbox.find('.form__message')).toHaveLength(1);
  });

  it('highlights when invalid', () => {
    const checkbox = shallow(
      <Field name="foo" onChange={jest.fn()} required="3" />
    );
    expect(checkbox.find('.form__field--invalid')).toHaveLength(1);
  });
});