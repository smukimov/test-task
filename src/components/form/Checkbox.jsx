import React from 'react';
import PropTypes from 'prop-types';

export class Checkbox extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    checked: PropTypes.bool,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  handleOnChange = ({target}) => {
    const value = target.checked;
    const {name} = this.props;
    this.props.onChange({name, value});
  };

  render() {
    const {label, checked, name} = this.props;

    return (
      <div className="form__field">
        {label && <label className="form__label">{label}</label>}
        <div className="form__controls">
           <input className="input form-control checkbox" type="checkbox" name={name} checked={checked}
                  onChange={this.handleOnChange} />
        </div>
      </div>
    );
  }
}

