import {mount, shallow} from 'enzyme';
import {MerchantForm} from './MerchantForm';
import React from 'react';
import {Merchant} from '../../entities/merchant';
import {Field} from '../form/Field';
import {Checkbox} from '../form/Checkbox';
import {MemoryRouter} from 'react-router-dom';

describe('<MerchantForm />', () => {
  it('renders without crash', () => {
    const mForm = shallow(
      <MerchantForm merchant={new Merchant()} onSubmit={jest.fn()} />
    );
    expect(mForm.is('.MerchantForm')).toBeTruthy();
  });

  it('renders 4 <Field />', () => {
    const mForm = shallow(
      <MerchantForm merchant={new Merchant()} onSubmit={jest.fn()} />
    );
    expect(mForm.find(Field)).toHaveLength(4);
  });

  it('renders 1 <Checkbox />', () => {
    const mForm = shallow(
      <MerchantForm merchant={new Merchant()} onSubmit={jest.fn()} />
    );
    expect(mForm.find(Checkbox)).toHaveLength(1);
  });

  it('calls onSubmit', () => {
    const onSubmitCallback = jest.fn();
    const merchant = new Merchant({
      firstname: 'foo',
      lastname: 'bar',
      email: 'foo@bar.com',
    });
    const mForm = mount(
      <MemoryRouter>
        <MerchantForm merchant={merchant} onSubmit={onSubmitCallback} />
      </MemoryRouter>
    );
    mForm.find(MerchantForm).simulate('submit');
    expect(onSubmitCallback.mock.calls.length).toBe(1);
  });
});
