import {shallow} from 'enzyme';
import {MerchantList} from './MerchantList';
import React from 'react';
import {Merchant} from '../../entities/merchant';
import {BindLink} from '../global/BindLink';

describe('<MerchantList />', () => {
  it('renders without crash', () => {
    const mList = shallow(
      <MerchantList merchantList={[]} onDelete={jest.fn()} />
    );
    expect(mList.is('.MerchantList')).toBeTruthy();
  });

  it('renders 2 merchant in table', () => {
    const merchantList = [new Merchant({id: 1}), new Merchant({id: 2})];
    const mList = shallow(
      <MerchantList merchantList={merchantList} onDelete={jest.fn()} />
    );
    expect(mList.find('tbody tr')).toHaveLength(2);
  });

  it('calls onDelete', () => {
    const onDeleteCallback = jest.fn();
    const merchantList = [new Merchant({id: 1})];
    const mList = shallow(
      <MerchantList merchantList={merchantList} onDelete={onDeleteCallback} />
    );
    mList.find(BindLink).simulate('click');
    expect(onDeleteCallback.mock.calls).toHaveLength(1);
  });
});