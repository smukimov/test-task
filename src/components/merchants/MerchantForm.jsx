import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import {Merchant} from '../../entities/merchant';
import './MerchantForm.css';
import {Field} from '../form/Field';
import {Checkbox} from '../form/Checkbox';
import {Validator} from '../form/validators';

export class MerchantForm extends React.Component {
  static propTypes = {
    merchant: PropTypes.instanceOf(Merchant).isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  constructor(prop) {
    super(prop);
    const {merchant: {id, firstname, lastname, email, phone, hasPremium, avatarUrl}} = this.props;
    this.state = {
      id,
      firstname,
      lastname,
      email,
      phone,
      hasPremium,
      avatarUrl,
    };
  }

  validationMap = {
    firstname: '3',
    lastname: '3',
    email: 'email',
  };

  isFormValid() {
    return Object.keys(this.validationMap).every(k => {
      const required = this.validationMap[k];
      const value = this.state[k];
      return Validator[required](value);
    });
  }

  handleOnSubmit = e => {
    e.preventDefault();
    const {id, firstname, lastname, email, phone, hasPremium, avatarUrl} = this.state;
    if (this.isFormValid()) {
      const merchant = new Merchant({id, firstname, lastname, email, phone, hasPremium, avatarUrl, bids: []});
      this.props.onSubmit(merchant);
    }
  };

  handleOnChange = ({name, value}) => {
    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <form className="MerchantForm form" onSubmit={this.handleOnSubmit}>
        <fieldset className="fieldset">
          <legend>A Merchant Form</legend>

          <div className="form__field">
            <div className="form__controls form__non-editable avatar__container">
              <img src={this.props.merchant.avatarUrl} alt={this.props.merchant.fullName()} />
            </div>
          </div>

          <Field name="firstname" onChange={this.handleOnChange}
                 label="First Name" value={this.state.firstname} required={this.validationMap.firstname} />

          <Field name="lastname" onChange={this.handleOnChange}
                 label="Last Name" value={this.state.lastname} required={this.validationMap.lastname} />

          <Field name="email" onChange={this.handleOnChange}
                 label="Email" value={this.state.email} required={this.validationMap.email} />

          <Field name="phone" onChange={this.handleOnChange} label="Phone" value={this.state.phone} />

          <Checkbox name="hasPremium" onChange={this.handleOnChange} label="Premium" checked={this.state.hasPremium} />

          <div className="form__field btn-block">
            <div className="form__controls BtnGroup">
              <button className="btn btn--primary BtnGroup-item" disabled={!this.isFormValid()}>Submit</button>
              <Link to="/" className="btn BtnGroup-item">Cancel</Link>
            </div>
          </div>
        </fieldset>
      </form>
    );
  }
}

