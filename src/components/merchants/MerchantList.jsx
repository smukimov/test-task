import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import './MerchantList.css';
import {Merchant} from '../../entities/merchant';
import {BindLink} from '../global/BindLink';

export function MerchantList({merchantList, onDelete}) {
  return (
    <div className="MerchantList">
      <Link to='/merchants/create' className="btn btn--primary">Create</Link>
      <table className="table">
        <thead>
        <tr>
          <th></th>
          <th>First name</th>
          <th>Last name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>#Bids</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        {merchantList.map(m =>
          <tr key={m.id}>
            <td>
              <div className="avatar__container">
                <img src={m.avatarUrl} alt={m.fullName()} />
              </div>
            </td>
            <td>{m.firstname}</td>
            <td>{m.lastname}</td>
            <td>{m.email}</td>
            <td>{m.phone}</td>
            <td>
               <Link to={`/merchants/${m.id}/bids`} className="btn social-count">{m.bids.length}</Link>
            </td>
            <td>
              <div className="BtnGroup">
                <Link to={`/merchants/edit/${m.id}`} className="btn BtnGroup-item">Edit</Link>
                <BindLink onClick={onDelete} value={m.id} className="btn btn--danger BtnGroup-item">Delete</BindLink>
              </div>
            </td>
          </tr>
        )}
        </tbody>
      </table>
    </div>
  );
}

MerchantList.propTypes = {
  merchantList: PropTypes.arrayOf(PropTypes.instanceOf(Merchant)),
  onDelete: PropTypes.func.isRequired,
};
