import {connect} from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';

import {BidList} from '../../components/bids/BidList';
import {Bid} from '../../entities/bid';
import {bidFetchRequest} from '../../redux/actions/bidActions';

class BidListPage extends React.Component {
  static propTypes = {
    bidList: PropTypes.arrayOf(PropTypes.instanceOf(Bid).isRequired),
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.node,
      }).isRequired,
    }).isRequired,
    getBids: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.getBids({merchantId: this.getId()});
  }

  getId() {
    return parseInt(this.props.match.params.id, 10);
  }

  render() {
    const {bidList} = this.props;
    return <BidList bidList={bidList} />;
  }
}

function mapStateToProps({b: {bids}}) {
  return {
    bidList: bids,
  };
}
export default connect(mapStateToProps, {getBids: bidFetchRequest})(BidListPage);
