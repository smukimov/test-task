import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {Merchant} from '../../entities/merchant';
import {MerchantForm} from '../../components/merchants/MerchantForm';
import {merchantGetRequest, merchantUpdateRequest} from '../../redux/actions/merchantActions';

class MerchantEditPage extends React.Component {

  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    getMerchant: PropTypes.func.isRequired,
    merchant: PropTypes.instanceOf(Merchant),
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.node,
      }).isRequired,
    }).isRequired,
    history: PropTypes.object.isRequired,
  };

  componentDidMount() {
    this.props.getMerchant({merchantId: this.getId()});
  }

  getId() {
    return parseInt(this.props.match.params.id, 10);
  }


  handleOnSubmit = merchant => {
    const {onSubmit, history} = this.props;
    onSubmit({merchantId: this.getId(), merchant});
    history.goBack();
  };

  render() {
    const {merchant} = this.props;

    if (!merchant) {
      return <div>Almost there</div>;
    }
    return <MerchantForm merchant={merchant} onSubmit={this.handleOnSubmit} />;
  }
}

function mapStateToProps({m: {merchant}}) {
  return {
    merchant,
  };
}

const mapDispatchToProps = {onSubmit: merchantUpdateRequest, getMerchant: merchantGetRequest};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MerchantEditPage));
