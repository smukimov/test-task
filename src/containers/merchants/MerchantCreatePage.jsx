import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {merchantCreateRequest} from '../../redux/actions/merchantActions';
import {Merchant} from '../../entities/merchant';
import {MerchantForm} from '../../components/merchants/MerchantForm';


class MerchantCreatePage extends React.Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
  };

  handleOnSubmit = (merchant) => {
    const {onSubmit, history} = this.props;
    onSubmit({merchant});
    history.goBack();
  };

  render() {
    return <MerchantForm merchant={new Merchant()} onSubmit={this.handleOnSubmit} />;
  }
}

export default withRouter(connect(null, {onSubmit: merchantCreateRequest})(MerchantCreatePage));
