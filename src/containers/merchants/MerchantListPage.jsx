import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {MerchantList} from '../../components/merchants/MerchantList';
import {Merchant} from '../../entities/merchant';
import {Pagination} from '../../components/navigation/Pagination';

import {merchantDeleteRequest, merchantsFetchRequest} from '../../redux/actions/merchantActions';

class MerchantListPage extends React.Component {

  static propTypes = {
    merchants: PropTypes.arrayOf(PropTypes.instanceOf(Merchant)),
    currentPage: PropTypes.number.isRequired,
    totalPages: PropTypes.number.isRequired,
    merchantsFetch: PropTypes.func.isRequired,
    deleteMerchant: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const {merchantsFetch, currentPage} = this.props;
    merchantsFetch({pageNum: currentPage});
  }

  handleOnDelete = merchantId => {
    const {deleteMerchant} = this.props;
    deleteMerchant({merchantId});
  };

  render() {
    const {merchants, currentPage, totalPages, merchantsFetch} = this.props;
    return [
      <MerchantList key="merchantList-1" merchantList={merchants} onDelete={this.handleOnDelete} />,
      totalPages > 0
        && <Pagination key="pagination-1" currentPage={currentPage} totalPages={totalPages}
                       onPageChange={merchantsFetch} />,
    ];
  }
}

function mapStateToProps({m: {merchants, currentPage, totalPages}}) {
  return {
    merchants,
    currentPage,
    totalPages,
  };
}

const mapDispatchToProps = {
  deleteMerchant: merchantDeleteRequest,
  merchantsFetch: merchantsFetchRequest,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MerchantListPage));
