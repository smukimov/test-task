import React from 'react';
import {Provider} from 'react-redux';
import {Route, Switch} from 'react-router-dom';

import {configureStore} from '../redux/configureStore';
import './App.css';
import MerchantListPage from './merchants/MerchantListPage';
import MerchantEditPage from './merchants/MerchantEditPage';
import MerchantCreatePage from './merchants/MerchantCreatePage';
import BidListPage from './bids/BidListPage';
import logo from '../logo.png';

const store = configureStore();

export function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </div>
        <Switch>
          <Route exact path="/" component={MerchantListPage} />
          <Route path="/merchants/edit/:id/" component={MerchantEditPage} />
          <Route path="/merchants/create" component={MerchantCreatePage} />
          <Route path="/merchants/:id/bids" component={BidListPage} />
        </Switch>
      </div>
    </Provider>
  );
}
