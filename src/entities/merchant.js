export class Merchant {
  constructor({id, firstname = '', lastname = '', avatarUrl = '', email = '', phone = '', hasPremium = false, bids = []} = {}) {
    this.id = id;
    this.firstname = firstname;
    this.lastname = lastname;
    this.avatarUrl = avatarUrl;
    this.email = email;
    this.phone = phone;
    this.hasPremium = hasPremium;
    this.bids = bids;
  }

  fullName() {
    return `${this.firstname} ${this.lastname}`;
  }
}

