export class Bid {
  constructor({id, carTitle = '', amount = 0, created = ''} = {}) {
    this.id = id;
    this.carTitle = carTitle;
    this.amount = amount;
    this.created = created;
  }
}
